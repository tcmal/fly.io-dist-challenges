#![feature(return_position_impl_trait_in_trait)]
use std::{future::Future, sync::Arc};

use common::{
    msg::*,
    msg_id::{gen_msg_id, MessageID},
    run_server, Handler,
};
use serde::{Deserialize, Serialize};

fn main() {
    run_server::<EchoHandler>();
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum EchoBody {
    #[serde(rename = "echo")]
    Echo { msg_id: MessageID, echo: String },

    #[serde(rename = "echo_ok")]
    EchoOk {
        msg_id: MessageID,
        in_reply_to: MessageID,
        echo: String,
    },
}

pub struct EchoHandler {
    output: Output<EchoBody>,
}

impl Handler for EchoHandler {
    type Body = EchoBody;

    fn init(_node_id: String, _node_ids: Vec<String>, output: Output<Self::Body>) -> Self {
        EchoHandler { output }
    }

    fn handle<'a>(
        self: &'a Arc<Self>,
        header: MessageHeader,
        body: Self::Body,
    ) -> impl Future<Output = ()> + Send + 'a {
        async move {
            match body {
                EchoBody::Echo { msg_id, echo } => {
                    self.output
                        .send(
                            &header.src,
                            &EchoBody::EchoOk {
                                msg_id: gen_msg_id(),
                                in_reply_to: msg_id,
                                echo,
                            },
                        )
                        .await;
                }
                EchoBody::EchoOk { .. } => (),
            };
        }
    }
}
