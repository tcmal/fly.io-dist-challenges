#![feature(return_position_impl_trait_in_trait)]

use common::run_server;

mod batch;
mod handler;
mod msg;
mod topology;

use handler::BroadcastHandler;

fn main() {
    run_server::<BroadcastHandler>();
}
