use common::msg_id::MessageID;
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};

pub type BroadcastTarget = usize;

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(tag = "type")]
pub enum BroadcastBody {
    #[serde(rename = "broadcast")]
    Broadcast {
        msg_id: Option<MessageID>,
        message: BroadcastTarget,
    },

    #[serde(rename = "broadcast_batch")]
    BroadcastBatch {
        msg_id: Option<MessageID>,
        messages: Vec<BroadcastTarget>,
    },

    #[serde(rename = "broadcast_ok")]
    BroadcastOk { in_reply_to: MessageID },

    #[serde(rename = "topology")]
    Topology {
        msg_id: Option<MessageID>,
        topology: HashMap<String, HashSet<String>>,
    },

    #[serde(rename = "topology_ok")]
    TopologyOk { in_reply_to: MessageID },

    #[serde(rename = "read")]
    Read { msg_id: MessageID },

    #[serde(rename = "read_ok")]
    ReadOk {
        in_reply_to: MessageID,
        messages: HashSet<BroadcastTarget>,
    },
}
