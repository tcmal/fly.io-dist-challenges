use std::collections::{HashMap, HashSet};

use smol::lock::RwLock;

pub type NodeId = String;
pub type TopologyDesc = HashMap<NodeId, HashSet<NodeId>>;

pub struct Topology(RwLock<TopologyDesc>);

impl Topology {
    /// Create a new topology in which all nodes are connected to each other.
    pub fn dense(node_ids: Vec<String>) -> Self {
        let mut top = TopologyDesc::new();
        for node_id in node_ids.iter() {
            top.insert(node_id.clone(), node_ids.iter().cloned().collect());
        }

        Topology(RwLock::new(top))
    }

    /// Replace the current topology with a new one.
    pub async fn replace(&self, new: TopologyDesc) {
        *self.0.write().await = new;
    }

    pub async fn all_targets(&self, node_id: &NodeId) -> HashSet<String> {
        self.0.read().await.get(node_id).unwrap().clone()
    }
}
