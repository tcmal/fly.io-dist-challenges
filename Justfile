test-echo:
    cargo build --bin echo
    maelstrom test -w echo --bin target/debug/echo --node-count 1 --time-limit 10

test-unique-ids:
    cargo build --bin unique_ids
    maelstrom test -w unique-ids --bin target/debug/unique_ids --time-limit 30 --rate 1000 --node-count 3 --availability total --nemesis partition

test-broadcast-single:
    cargo build --bin broadcast
    maelstrom test -w broadcast --bin target/debug/broadcast --node-count 1 --time-limit 20 --rate 10

test-broadcast-multi:
    cargo build --bin broadcast
    maelstrom test -w broadcast --bin target/debug/broadcast --node-count 5 --time-limit 20 --rate 10

test-broadcast-fault:
    cargo build --bin broadcast
    maelstrom test -w broadcast --bin target/debug/broadcast --node-count 5 --time-limit 20 --rate 10 --nemesis partition

test-broadcast-efficient:
    cargo build --bin broadcast
    maelstrom test -w broadcast --bin target/debug/broadcast --node-count 25 --time-limit 20 --rate 100 --latency 100
    sed -n '/:net/{:start /.*:valid? true}/!{N;b start};/.*/p}' store/latest/results.edn
    sed -n '/:stable-latencies/{:start /.*1.*}/!{N;b start};/.*/p}' store/latest/results.edn

test: test-echo test-unique-ids test-broadcast-single test-broadcast-multi