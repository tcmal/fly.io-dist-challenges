use std::time::{SystemTime, UNIX_EPOCH};

use rand::{thread_rng, Rng};

pub type MessageID = u64;

pub fn gen_msg_id() -> MessageID {
    // Time since UNIX epoch in milliseconds, (48 bits)
    let now = SystemTime::now();
    let time_millis: u128 = now.duration_since(UNIX_EPOCH).unwrap().as_millis();

    // 16 bits of randomness
    let rand: u16 = thread_rng().gen();

    ((time_millis as u64) << 16) | (rand as u64)
}
